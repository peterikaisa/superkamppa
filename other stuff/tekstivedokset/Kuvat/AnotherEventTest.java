import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;


public class AnotherEventTest extends JFrame{

		Button button;
		boolean toggle;
		
		public AnotherEventTest() {
			button = new Button();
			button.setLabel("Nappi");
			setSize(100, 100);
			button.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					if (toggle) button.setLabel("Klik");
					else button.setLabel("Klik klik");
					
					toggle = !toggle;
				}
			});
			add(button);
			toggle = true;
			
		}
		
		public static void main(String[] args) {
			AnotherEventTest test = new AnotherEventTest();
			test.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			test.setVisible(true);
		}

	}