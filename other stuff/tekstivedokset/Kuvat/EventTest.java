import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;


public class EventTest extends JFrame implements ActionListener{

	Button button;
	boolean toggle;
	
	public EventTest() {
		button = new Button();
		button.setLabel("Nappi");
		button.addActionListener(this);
		setSize(100, 100);
		add(button);
		toggle = true;
	}
	
	public static void main(String[] args) {
		EventTest test = new EventTest();
		test.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		test.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (toggle) button.setLabel("Klik");
		else button.setLabel("Klik klik");
		
		toggle = !toggle;
	}

}
