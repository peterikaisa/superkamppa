Tämä harjoitustyö liittyy Turun yliopiston Käyttöliittymät-kurssiin. :penguin:

## Dokumentaatio

Harjoitustyöstä löytyy javadoc-dokumentaatio [documentation](documentation/index.html)-kansiosta.

## Sovelluksen asentaminen

Asentamista varten tarvitset Android-laitteen tai emulaattorin. Emulaattoreista ohjeistus koskee lähinnä Android Studion emulaattorilla ajamista, muiden emulaattoreiden käytöstä käyttäjä on itse vastuussa.

* Emulaattori (vaatimuksena Android Studio)

Lataa itsellesi git-projekti zip-tiedostona tai tallenna itsellesi kopio (fork).
Avaa Android Studio ja sieltä tämä projekti File > Open > 'valitse projekti' > OK.
Jos sinulla ei ole vielä emulaattoriprofiilia, voit luoda sen [näin](http://developer.android.com/tools/devices/managing-avds.html).
Voit ajaa ohjelman emulaattorilla valitsemalla Run > Run 'app' > 'valitse emulaattorisi' > OK, sovellus avautuu emulaattoriin.

* Android-laite (vaatimuksena noin 4 MB tilaa)

Lataa laitteeseesi [release](release)-kansiosta löytyvä .apk-tiedosto. Tätä varten sinun on sallittava puhelimesi asetuksista tuntemattomat lähteet. Ohjeita [tässä](http://www.cnet.com/how-to/how-to-install-apps-outside-of-google-play/).
Latauksen jälkeen voit asentaa sovelluksen klikkaamalla .apk-tiedostoa.
Sovelluksen käyttöohjeet löytyvät myös release-kansiosta.