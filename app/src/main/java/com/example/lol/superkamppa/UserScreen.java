package com.example.lol.superkamppa;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by kaisa on 2.3.2016.
 * Modified by Veera on 19.3.2016
 */
public class UserScreen extends ListActivity {

    private HouseArrayAdapter houseADP;
    private String currentUser;



    public final static String EXTRA_USER = "com.example.lol.superkamppa.USER";
    public final static String EXTRA_HOUSE = "com.example.lol.superkamppa.HOUSE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_user);

        currentUser = UserScreen.this.getIntent().getStringExtra(MainActivity.EXTRA_USER);

        DBhelper dBhelper = DBhelper.getInstance(UserScreen.this);
        String[] houses = dBhelper.getHouses(currentUser); //Fetch houses from database

        houseADP = new HouseArrayAdapter(UserScreen.this, houses); //Create a new adapter for listView
        UserScreen.this.setListAdapter(houseADP); //Add adapter to listView / house list
    }

    /**
     * Called after UserScreen has been stopped, prior to it being started again.
     * Notifies adapter to refresh for possible data changes.
     */
    @Override
    protected void onRestart(){
        super.onRestart();
        houseADP.notifyDataSetChanged();
    }

    /**------------------CUSTOM ADAPTER CLASS-----------------------*/

    /**
     * Custom adapter for filling the list of houses of the current user.
     */
    private class HouseArrayAdapter extends ArrayAdapter<String> {

        private final Activity context;
        private final String[] houses;

        /**
         * Creates a new HouseArrayAdapter.
         * In adapting items into listView it uses houselist_item_layout as each row's layout.
         * @param context current activity
         * @param values  array with house names
         */
        public HouseArrayAdapter(Activity context, String[] values) {
            super(context, R.layout.houselist__item_layout, values);
            this.context = context;
            this.houses = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rowView = convertView;

            //Reusing rows: if user has more houses than the screen can fit, scrolling is required and
            //when user scrolls the house list, shown rows are reused and filled with new information.
            if (rowView == null) {
                LayoutInflater inflater = context.getLayoutInflater();
                rowView = inflater.inflate(R.layout.houselist__item_layout, null);
                TextView houseName = (TextView) rowView.findViewById(R.id.house_name_textview);
                rowView.setTag(houseName);
            }

            //Add onClickListener to rows
            rowView.setOnClickListener(new View.OnClickListener() {
                /**
                 * Called when user clicks an item/house in the list. Opens house screen.
                 * @param rowView The view object (ListView's child view / row) that was clicked. HouseListAdapter provides these views.
                 */
                @Override
                public void onClick(View rowView) {
                    final TextView houseText = (TextView) rowView.findViewById(R.id.house_name_textview); //House's name
                    final String houseName = houseText.getText().toString();

                    Intent intent = new Intent(UserScreen.this, HouseScreen.class);
                    intent.putExtra(EXTRA_USER, currentUser);
                    intent.putExtra(EXTRA_HOUSE, houseName);
                    startActivity(intent); //Starts and opens house screen for the house that was clicked
                }
            });

            //Fill textView in row layout with new house name
            TextView newHouseName = (TextView) rowView.getTag();
            String name = houses[position];
            newHouseName.setText(name);

            return rowView;
        }
    }
}
