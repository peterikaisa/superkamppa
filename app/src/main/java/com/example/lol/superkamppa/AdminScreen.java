package com.example.lol.superkamppa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


/**
 * Created by kaisa on 2.3.2016.
 */
public class AdminScreen extends AppCompatActivity{

    private DBhelper dBhelper;
    private String currentUser;
    public final static String EXTRA_USER = "com.example.lol.superkamppa.USER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_admin);

        dBhelper = DBhelper.getInstance(this);
        Intent intent = getIntent();
        currentUser = intent.getStringExtra(MainActivity.EXTRA_USER);
    }

    /**
     * Add new user.
     * @param view
     */
    public void addUser(View view) {
        Intent intent = new Intent(this, CreateUserScreen.class);
        startActivity(intent);
    }

    /**
     * Removes user.
     * @param view
     */
    public void removeUser(View view) {
        EditText removeUser = (EditText) findViewById(R.id.user_to_remove);
        if (removeUser == null || removeUser.equals("")) { //Don't allow empty queries to database
            Toast.makeText(AdminScreen.this, "Kenttä ei voi olla tyhjä", Toast.LENGTH_LONG).show();
        }
        boolean removeSuccess = dBhelper.removeUser(removeUser.getText().toString());
        if (removeSuccess) {
            Toast.makeText(AdminScreen.this, "Poisto onnistui", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(AdminScreen.this, "Poisto epäonnistui", Toast.LENGTH_LONG).show();
        }
    }

    public void modifyUser(View view) {
        EditText modifyUser = (EditText) findViewById(R.id.user_to_modify);
        if (modifyUser == null || modifyUser.equals(""))
            Toast.makeText(AdminScreen.this, "Kenttä ei voi olla tyhjä", Toast.LENGTH_LONG).show();
        String user = modifyUser.getText().toString();
        Intent intent = new Intent(this, ModifyUsersActivity.class);
        intent.putExtra(EXTRA_USER, user);
        startActivity(intent);
    }
}