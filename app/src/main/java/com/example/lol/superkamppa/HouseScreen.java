package com.example.lol.superkamppa;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

/**
 * Created by Veera on 20.3.2016.
 */
public class HouseScreen extends AppCompatActivity {

    DBhelper dBhelper;
    private String[] rooms;
    private String currentUser;
    private String currentHouse;

    public final static String EXTRA_USER = "com.example.lol.superkamppa.USER";
    public final static String EXTRA_HOUSE = "com.example.lol.superkamppa.HOUSE";
    public final static String EXTRA_ROOM = "com.example.lol.superkamppa.ROOM";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_house);
        currentUser = HouseScreen.this.getIntent().getStringExtra(UserScreen.EXTRA_USER);
        currentHouse = HouseScreen.this.getIntent().getStringExtra(UserScreen.EXTRA_HOUSE);
        ((SeekBar)findViewById(R.id.house_heat_bar)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress % 5 == 0) Toast.makeText(getApplicationContext(), String.valueOf(progress), Toast.LENGTH_SHORT).show();
                dBhelper.modifyHouseObjs(currentHouse, false, 3, progress);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
    }

    /**
     * Called after HouseScreen has been stopped, prior to it being started again.
     * In other words, called after user return from room screen.
     * Creates a new PopulateScreenTask and executes it - for possible data changes.
     */
    @Override
    protected void onRestart(){
        super.onRestart();
    }

    @Override
    protected void onStart() {
        super.onStart();
        dBhelper = DBhelper.getInstance(HouseScreen.this);
        rooms = dBhelper.getRooms(currentHouse); //Fetch houses from database
        final Button room1Button = (Button) findViewById(R.id.button_open_room1); //Find buttons
        final Button room2Button = (Button) findViewById(R.id.button_open_room2);
        room1Button.setText(rooms[0]); //Fill button texts with room names
        room2Button.setText(rooms[1]);

        //Set overall house power buttons and heating
        boolean allPower = dBhelper.getAllPowerHouse(currentHouse);
        ((ToggleButton)findViewById(R.id.house_power_button)).setChecked(allPower);
        boolean allLights = dBhelper.getAllLightsHouse(currentHouse);
        ((ToggleButton)findViewById(R.id.house_lights_button)).setChecked(allLights);
        int heating = dBhelper.getAllHeat(currentHouse);
        ((SeekBar) findViewById(R.id.house_heat_bar)).setProgress(heating);
    }

    /**
     * Opens room screen for the room, which button was clicked.
     * @param view
     */
    public void openRoomScreen(View view) {
        Intent intent = new Intent(this, RoomScreen.class);
        intent.putExtra(EXTRA_USER, currentUser);
        intent.putExtra(EXTRA_HOUSE, currentHouse);
        final Button roomButton;
        final String roomName;
        //Determines which room's button was clicked
        switch (view.getId()) {
            case(R.id.button_open_room1):
                roomButton = (Button) findViewById(R.id.button_open_room1);
                roomName = roomButton.getText().toString();
                intent.putExtra(EXTRA_USER, currentUser);
                intent.putExtra(EXTRA_HOUSE, currentHouse);
                intent.putExtra(EXTRA_ROOM, roomName);
                break;
            case(R.id.button_open_room2):
                roomButton = (Button) findViewById(R.id.button_open_room2);
                roomName = roomButton.getText().toString();
                intent.putExtra(EXTRA_USER, currentUser);
                intent.putExtra(EXTRA_HOUSE, currentHouse);
                intent.putExtra(EXTRA_ROOM, roomName);
                break;
        }
        startActivity(intent);
    }

    /**
     * Called when user pushes change_houseName_button in house screen.
     * @param view
     */
    public void changeHouseName(View view) {

        //Create a dialog to show for users
        final Dialog dialog = new Dialog(this);
        dialog.setTitle(R.string.dialog_housename_title);
        dialog.setContentView(R.layout.dialog_text);
        dialog.show();
        EditText input = (EditText) dialog.findViewById(R.id.users_input);
        input.setHint(R.string.change_housename_hint);

        Button ok = (Button) dialog.findViewById(R.id.ok_button);
            ok.setOnClickListener(new View.OnClickListener() {
                /**
                 * When OK button is clicked, try to change house name by calling changeHousename()
                 * in DBhelper to current database instance.
                 * @param v view
                 */
                @Override
                public void onClick(View v) {
                    final TextView housename = (TextView) dialog.findViewById(R.id.users_input);
                    final String newHousename = housename.getText().toString();
                    boolean updateSuccess = dBhelper.changeHousename(currentHouse, newHousename);
                    if (updateSuccess) {
                        Toast.makeText(HouseScreen.this, "Nimi muutettu!", Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    } else {
                        Toast.makeText(HouseScreen.this, "Nimen vaihtaminen epäonnistui!", Toast.LENGTH_LONG).show();
                    }
                }
            });

        Button cancel = (Button) dialog.findViewById(R.id.cancel_button);
        cancel.setOnClickListener(new View.OnClickListener() {
            /**
             * When PERUUTA button is clicked, close dialog.
             * @param v view
             */
            @Override
            public void onClick(View v) {
                Toast.makeText(HouseScreen.this, "Kodin nimeä ei muutettu!", Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });
    }

    /**
     * Called when user pushes user_toHouse_button in house screen.
     * @param view
     */
    public void addUserToHouse(View view) {

        //Create a dialog to show for users
        final Dialog dialog = new Dialog(this);
        dialog.setTitle(R.string.dialog_addinguser);
        dialog.setContentView(R.layout.dialog_text);
        dialog.show();
        EditText input = (EditText) dialog.findViewById(R.id.users_input);
        input.setHint(R.string.dialog_username_hint);

        Button ok = (Button) dialog.findViewById(R.id.ok_button);
        ok.setOnClickListener(new View.OnClickListener() {
            /**
             * When OK button is clicked, try to add user, which was typed in
             * the input space, to current house.
             * @param v view
             */
            @Override
            public void onClick(View v) {
                final TextView username = (TextView) dialog.findViewById(R.id.users_input);
                final String newUser = username.getText().toString();
                boolean updateSuccess = dBhelper.addUserToHouse(currentHouse, newUser);
                if (updateSuccess) {
                    Toast.makeText(HouseScreen.this, "Käyttäjä lisätty!", Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                } else {
                    Toast.makeText(HouseScreen.this, "Lisäys epäonnistui!", Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            }
        });

        Button cancel = (Button) dialog.findViewById(R.id.cancel_button);
        cancel.setOnClickListener(new View.OnClickListener() {
            /**
             * When PERUUTA button is clicked, close dialog.
             * @param v
             */
            @Override
            public void onClick(View v) {
                Toast.makeText(HouseScreen.this, "Käyttäjää ei lisätty!", Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });
    }

    /**
     * Modifies
     * a) all house's lights to on/off  OR
     * b) all house's power to on/off  OR
     * @param view
     */
    public void modifyHouseObjects(View view) {
        //Determine if user changed lights or power or heating
        switch (view.getId()) {
            case(R.id.house_power_button): //MODIFY HOUSE POWER
                dBhelper.modifyHouseObjs(currentHouse, ((ToggleButton)findViewById(R.id.house_power_button)).isChecked(), 1, 0);
                break;
            case(R.id.house_lights_button): //MODIFY HOUSE LIGHTS
                dBhelper.modifyHouseObjs(currentHouse, ((ToggleButton)findViewById(R.id.house_lights_button)).isChecked(), 2, 0);
                break;
        }
    }
}