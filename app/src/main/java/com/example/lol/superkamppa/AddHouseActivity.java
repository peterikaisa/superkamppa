package com.example.lol.superkamppa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class AddHouseActivity extends AppCompatActivity {

    private DBhelper dBhelper;
    private String currentUser;

    public final static String EXTRA_USER = "com.example.lol.superkamppa.USER";

    @Override
    protected void onCreate(Bundle savedInstancestate) {
        super.onCreate(savedInstancestate);
        setContentView(R.layout.activity_add_house);

        dBhelper = DBhelper.getInstance(this);

        Intent intent = getIntent();
        currentUser = intent.getStringExtra(EXTRA_USER);
    }

    /**
     * Method to create a new house. Method is called from activity_add_house
     * @param view
     */
    public void createHouse (View view) {

        EditText houseNameInput = (EditText) findViewById(R.id.new_houseName_input);
        EditText firstRoomnameInput = (EditText) findViewById(R.id.new_room1name_input);
        EditText secondRoomnameInput = (EditText) findViewById(R.id.new_room2name_input);

        String newHouseName = houseNameInput.getText().toString();
        String newRoom1 = firstRoomnameInput.getText().toString();
        String newRoom2 = secondRoomnameInput.getText().toString();

        //Call addHouse method in DBhelper to try and create a new house with given parameters
        Boolean houseWasCreated = dBhelper.addHouse(newHouseName, currentUser, newRoom1, newRoom2);

        //User customized items:
        EditText room1CustomItem1 = (EditText) findViewById(R.id.room1_object1_input);
        EditText room1CustomItem2 = (EditText) findViewById(R.id.room1_object2_input);
        EditText room2CustomItem1 = (EditText) findViewById(R.id.room2_object1_input);
        EditText room2CustomItem2 = (EditText) findViewById(R.id.room2_object2_input);

        //First room's custom items' names
        String room1Item1Name = room1CustomItem1.getText().toString();
        String room1Item2Name = room1CustomItem2.getText().toString();

        //Second room's custom items' names
        String room2Item1Name = room2CustomItem1.getText().toString();
        String room2Item2Name = room2CustomItem2.getText().toString();

        Boolean defaultItemsCreated = dBhelper.addDefaultObjects(newRoom1, newRoom2);
        Boolean customItemsCreated = dBhelper.addItems(newRoom1, newRoom2, room1Item1Name, room1Item2Name, room2Item1Name, room2Item2Name);

        //If house with all its features was created and everything was updated to database
        if (houseWasCreated && defaultItemsCreated && customItemsCreated) {
            Toast.makeText(AddHouseActivity.this, "Talo luotu", Toast.LENGTH_LONG).show();
        }
        //If something went wrong, return back
        else {
            Toast.makeText(AddHouseActivity.this, "Talon luonti epäonnistui", Toast.LENGTH_LONG).show();
        }
    }
}
