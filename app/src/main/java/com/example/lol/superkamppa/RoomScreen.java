package com.example.lol.superkamppa;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

/**
 * Created by Veera on 20.3.2016.
 */
public class RoomScreen extends AppCompatActivity {

    private DBhelper dBhelper;

    private String currentUser;
    private String currentHouse;
    private String currentRoom;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_room);

        dBhelper = DBhelper.getInstance(this);

        Intent intent = getIntent();
        currentUser = intent.getStringExtra(UserScreen.EXTRA_USER);
        currentHouse = intent.getStringExtra(UserScreen.EXTRA_HOUSE);
        currentRoom = intent.getStringExtra(HouseScreen.EXTRA_ROOM);

        ((SeekBar) findViewById(R.id.room_heat_bar)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress % 5 == 0)Toast.makeText(getApplicationContext(), String.valueOf(progress), Toast.LENGTH_SHORT).show(); //show progress to user while scrolling bar
                dBhelper.modifyRoomObjs(currentRoom, false, 3, progress);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStart() {
        super.onStart();
        String[] items = dBhelper.getItems(currentRoom); //Fetch current room's items from database
        Button item1 = (Button) findViewById(R.id.item1); //Find item buttons
        Button item2 = (Button) findViewById(R.id.item2);
        //Set item power buttons pressed according to current values in database
        item1.setText(items[2]); //Fill item button texts with item names
        boolean item1power = dBhelper.getObjectValue(currentRoom, items[2]) == 1 ? true : false;
        ((ToggleButton)findViewById(R.id.item1_power_button)).setChecked(item1power);
        item2.setText(items[3]);
        boolean item2power = dBhelper.getObjectValue(currentRoom, items[3]) == 1 ? true : false;
        ((ToggleButton)findViewById(R.id.item2_power_button)).setChecked(item2power);
        boolean lights = dBhelper.getObjectValue(currentRoom, "Valot") == 1 ? true : false;
        ((ToggleButton)findViewById(R.id.room_lights_button)).setChecked(lights);
        int heating = dBhelper.getObjectValue(currentRoom, "Lämmitys");
        ((SeekBar) findViewById(R.id.room_heat_bar)).setProgress(heating);
        //If all items are "on" set master switch to "on"
        boolean allPower = dBhelper.getAllPowerRoom(currentRoom);
        ((ToggleButton)findViewById(R.id.room_power_button)).setChecked(allPower);
    }

    /**
     * Method for changing rooms name.
     * Opens a dialog where user can type a new name for the room.
     * @param view v
     */
    public void changeRoomName(View view) {
        final Dialog dialog = new Dialog(this);
        dialog.setTitle(R.string.dialog_roomname_title);
        dialog.setContentView(R.layout.dialog_text);
        dialog.show();
        EditText input = (EditText) dialog.findViewById(R.id.users_input);
        input.setHint(R.string.change_roomname_hint);

        Button ok = (Button) dialog.findViewById(R.id.ok_button);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TextView usersInput = (TextView) dialog.findViewById(R.id.users_input);
                final String newRoomname = usersInput.getText().toString();
                boolean updateSuccess = dBhelper.changeRoomname(currentHouse, currentRoom, newRoomname);
                if (updateSuccess) {
                    Toast.makeText(RoomScreen.this, "Nimi muutettu!", Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                } else {
                    Toast.makeText(RoomScreen.this, "Nimen muuttaminen epäonnistui!", Toast.LENGTH_LONG).show();
                }
            }
        });
        Button cancel = (Button) dialog.findViewById(R.id.cancel_button);
        cancel.setOnClickListener(new View.OnClickListener() {
            /**
             * When PERUUTA button is clicked, close dialog.
             * @param v view
             */
            @Override
            public void onClick(View v) {
                Toast.makeText(RoomScreen.this, "Huoneen nimeä ei muutettu!", Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });
    }

    /**
     * Method for changing an item's name.
     * @param view v
     */
    public void changeItemName(View view) {
        final Dialog dialog = new Dialog(this);
        dialog.setTitle(R.string.dialog_itemname_title);
        dialog.setContentView(R.layout.dialog_text);
        dialog.show();
        final EditText input = (EditText) dialog.findViewById(R.id.users_input);
        input.setHint(R.string.change_roomname_hint);

        Button ok = (Button) dialog.findViewById(R.id.ok_button);
        ok.setOnClickListener(new View.OnClickListener() {
            /**
             * When OK button is clicked, try to change item's name by calling changeItemName()
             * in DBhelper to current database instance.
             * @param v view
             */
            @Override
            public void onClick(View v) {
                //Determine which item's button was clicked and according to that
                //try to change item's name
                switch (v.getId()) {
                    case (R.id.item1):
                        final Button item = (Button) dialog.findViewById(R.id.item1);
                        String oldItemName = item.getText().toString();
                        String newItemName = input.getText().toString();
                        boolean updateSuccess = dBhelper.changeItemname(currentRoom, oldItemName, newItemName);
                        if (updateSuccess) {
                            Toast.makeText(RoomScreen.this, "Nimi muutettu!", Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                            break;
                        } else {
                            Toast.makeText(RoomScreen.this, "Nimen vaihtaminen epäonnistui!", Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                            break;
                        }
                    case (R.id.item2):
                        final Button item2 = (Button) dialog.findViewById(R.id.item2);
                        String oldItem2Name = item2.getText().toString();
                        String newItem2name = input.getText().toString();
                        boolean success = dBhelper.changeItemname(currentRoom, oldItem2Name, newItem2name);
                        if (success) {
                            Toast.makeText(RoomScreen.this, "Nimi muutettu!", Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                            break;
                        } else {
                            Toast.makeText(RoomScreen.this, "Nimen vaihtaminen epäonnistui!", Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                            break;
                        }
                }
            }
        });

        Button cancel = (Button) dialog.findViewById(R.id.cancel_button);
        cancel.setOnClickListener(new View.OnClickListener() {
            /**
             * When PERUUTA button is clicked, close dialog.
             * @param v view
             */
            @Override
            public void onClick(View v) {
                Toast.makeText(RoomScreen.this, "Huoneen nimeä ei muutettu!", Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });
    }

    /**
     * Modifies
     * a) all room's lights to on/off  OR
     * b) all room's power to on/off  OR
     * @param view
     */
    public void modifyRoomObjects(View view) {
        //Determine if user changed lights or power
        switch (view.getId()) {
            case(R.id.room_power_button): //MODIFY ROOM POWER
                dBhelper.modifyRoomObjs(currentRoom, ((ToggleButton)findViewById(R.id.room_power_button)).isChecked(), 1, 0);
                break;
            case(R.id.room_lights_button): //MODIFY ROOM  LIGHTS
                dBhelper.modifyRoomObjs(currentRoom, ((ToggleButton)findViewById(R.id.room_lights_button)).isChecked(), 2, 0);
                break;
        }
    }
}

