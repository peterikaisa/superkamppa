package com.example.lol.superkamppa;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by kkeikei on 14.3.2016.
 */
public class ModifyUsersActivity extends AppCompatActivity{

    private DBhelper dBhelper;
    private String userModify;

    @Override
    protected void onCreate(Bundle savedInstancestate) {
        super.onCreate(savedInstancestate);
        setContentView(R.layout.modify_users);

        dBhelper = DBhelper.getInstance(this);
        Intent intent = getIntent();
        userModify = intent.getStringExtra(AdminScreen.EXTRA_USER);
    }

    /**
     * Removes houses from given user. Is called from modify_users. User is given from last activity
     * as an extra.
     */
    public void removeHouses(View view) {
        final String[] usersHouses = dBhelper.getHouses(userModify);
        final ArrayList<String> deleteHouses = new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Poista taloja")
            .setMultiChoiceItems(usersHouses, null, new DialogInterface.OnMultiChoiceClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                    if (isChecked) deleteHouses.add(usersHouses[which]);
                    else if (deleteHouses.contains(usersHouses[which])) deleteHouses.remove(usersHouses[which]);
                }
            })
            .setPositiveButton("Poista talot", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    boolean success = dBhelper.deleteHouses(userModify, deleteHouses);
                    if (success) {
                        Toast.makeText(ModifyUsersActivity.this, "Talot poistettu", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(ModifyUsersActivity.this, "Taloja ei poistettu", Toast.LENGTH_LONG).show();
                    }
                }
            })
                .setNegativeButton("Poistu", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(ModifyUsersActivity.this, "Taloja ei poistettu", Toast.LENGTH_LONG).show();
                    }
                })
            .show();
    }


    /**
     * Adds house to given user. Is called from modify_users add_houses_button
     * @param view
     */
    public void addHouse(View view) {
        Intent intent = new Intent(this, AddHouseActivity.class);
        intent.putExtra(AddHouseActivity.EXTRA_USER, userModify);
        startActivity(intent);
    }

    /**
     * Changes password from given user. Username and passwords will be taken from modify_users
     */
    public void changePassword(View view) {
        EditText user = (EditText) findViewById(R.id.username_change);
        EditText firstPassword = (EditText) findViewById(R.id.new_password);
        EditText secondPassword = (EditText) findViewById(R.id.new_password_second);
        String username = user.getText().toString(), passw = firstPassword.getText().toString(),
                passw2 = secondPassword.getText().toString();
        if (passw.equals(passw2) && username.equals(userModify)) {
            boolean success = dBhelper.updatePassword(userModify, passw);
            if (success) Toast.makeText(ModifyUsersActivity.this, "Salasana päivitetty", Toast.LENGTH_LONG).show();
            else Toast.makeText(ModifyUsersActivity.this, "Salasanaa ei päivitetty", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(ModifyUsersActivity.this, "Salasanaa ei päivitetty", Toast.LENGTH_LONG).show();
        }
    }
}
