package com.example.lol.superkamppa;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;

/**
 * Created by kaisa on 2.3.2016.
 */
public class DBhelper extends SQLiteOpenHelper{

    private static DBhelper instance;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "HouseDatabase.db";
    /* tables */
    private static final String TABLE_USERS = "USERS";
    private static final String TABLE_HOUSES = "HOUSES";
    private static final String TABLE_ITEMS = "ITEMS";
    /* table columns */
    private static final String USERS_USERNAME = "username";
    private static final String USERS_PASSWORD = "password";
    private static final String USERS_ADMIN = "admin";
    private static final String HOUSES_HOUSENAME = "housename";
    private static final String HOUSES_USER = "user";
    private static final String HOUSES_ROOM = "room";
    private static final String ITEMS_ITEMNAME = "itemname";
    private static final String ITEMS_ROOM = "room";
    private static final String ITEMS_VALUE = "value";

    /* create table x */
    private static final String CREATE_TABLE_USERS = "CREATE TABLE " + TABLE_USERS
            + "(" + USERS_USERNAME + " TEXT PRIMARY KEY," + USERS_PASSWORD + " BLOB NOT NULL,"
            + USERS_ADMIN + " BOOLEAN NOT NULL)";
    private static final String CREATE_TABLE_HOUSES = "CREATE TABLE " + TABLE_HOUSES
            + "(" + HOUSES_HOUSENAME + " TEXT NOT NULL," + HOUSES_USER + " TEXT NOT NULL," + HOUSES_ROOM
            + " TEXT NOT NULL, PRIMARY KEY (" + HOUSES_HOUSENAME + ", " + HOUSES_USER + ", " + HOUSES_ROOM + "))";
    private static final String CREATE_TABLE_ITEMS = "CREATE TABLE " + TABLE_ITEMS
            + "(" + ITEMS_ITEMNAME + " TEXT ," + ITEMS_ROOM + " TEXT,"
            + ITEMS_VALUE + " BLOB NOT NULL, PRIMARY KEY (" + ITEMS_ITEMNAME + ", " + ITEMS_ROOM + "))";

    /**
     * Make sure database is initialized only once. After that we will always return existing
     * instances of it. Instances of database are only available via this (getInstance) method.
     * @return instance of database
     */
    public static synchronized DBhelper getInstance(Context context) {
        if (instance == null) instance = new DBhelper(context.getApplicationContext());
        return instance;
    }

    private DBhelper (Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_USERS);
        db.execSQL(CREATE_TABLE_HOUSES);
        db.execSQL(CREATE_TABLE_ITEMS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer) {
        db.execSQL("DROP TABLE IF EXISTS USERS");
        db.execSQL("DROP TABLE IF EXISTS HOUSES");
        db.execSQL("DROP TABLE IF EXISTS ITEMS");
        onCreate(db);
    }


    /** ----------------- USER RELATED ------------------- */

    /**
     * Checks given credentials against database. If user is found with matching username
     * and password, true-value is returned. Also checks if this user is an admin and returns
     * a true-value also, if this is the case.
     * @param username username to check
     * @param password password to check
     * @return returns an array of length two containing boolean values. In the first column is a
     * confirmation of whether this user has been registered in database. There is information
     * about users admin-rights in the second column.
     */
    public boolean[] checkLogin(String username, String password) {
        boolean userFound[] = {false, false};

        //First, make the sql-query
        SQLiteDatabase readable = instance.getReadableDatabase();
        String[] columns = {USERS_USERNAME, USERS_PASSWORD, USERS_ADMIN};
        String selection = USERS_USERNAME + " LIKE ? AND " + USERS_PASSWORD + " LIKE ?";
        String[] selectionArgs = {username, password};
        Cursor query = readable.query(TABLE_USERS, columns, selection, selectionArgs, null, null, null);
        query.moveToFirst();

        if (query.getCount() == 0) return userFound; //if nothing is found on database, user obviously doesn't exist
        else userFound[0] = true;

        if (query.getInt(query.getColumnIndex(USERS_ADMIN)) == 1) userFound[1] = true; //if this user is an admin
        query.close();
        return userFound;
    }

    /**
     * Checks whether an user with this name exists. Returns boolean to indicate answer.
     * @param username user's username which is checked
     * @return true if user exists
     */
    public boolean checkLogin(String username) {
        boolean userFound = false;

        SQLiteDatabase readable = instance.getReadableDatabase();
        String columns[] = {USERS_USERNAME};
        String selection = USERS_USERNAME + " LIKE ? ";
        String[] selectionArgs = {username};
        Cursor query = readable.query(TABLE_USERS, columns, selection, selectionArgs, null, null, null);
        query.moveToFirst();

        if (query.getCount() == 0) return userFound; //if database is empty, return
        String name = query.getString(query.getColumnIndex(USERS_USERNAME));
        if (name != null && name.equals(username)) userFound = true; //if this user already exists
        query.close();
        return userFound;
    }

    /**
     * Checks if device has an admin already.
     * @return true if admin exists
     */
    public boolean queryAdmin() {
        boolean adminExists = false;
        SQLiteDatabase readable = instance.getReadableDatabase();
        String[] columns = {USERS_USERNAME, USERS_PASSWORD, USERS_ADMIN};
        String selection = USERS_ADMIN + " LIKE ?";
        String[] selectionArgs = {"1"};
        Cursor query = readable.query(TABLE_USERS, columns, selection, selectionArgs, null, null, null);
        query.moveToFirst();
        if (query.getCount() > 0) adminExists = true;
        query.close();
        return adminExists;
    }

    /**
     * Adds a new user into database, but first checks if user already exists in database.
     * So, no duplicates allowed. Even admins can't have two accounts with admin-rights and
     * without.
     * @param username user to be created
     * @param password users password
     * @param admin if user will be admin
     * @return If adding a new user was successful returns true, else returns false
     */
    public boolean addUser(String username, String password, boolean admin) {
        boolean[] userAlreadyExists = this.checkLogin(username, password);
        if (userAlreadyExists[0]) return false; //if this user already exists
        if (admin && queryAdmin()) return false; //Cannot add this user as admin as there is already an admin

        SQLiteDatabase writable = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(USERS_USERNAME, username);
        values.put(USERS_PASSWORD, password);
        values.put(USERS_ADMIN, admin);
        long newId = writable.insert(TABLE_USERS, null, values);

        return (newId != -1);
    }

    /**
     * Removes data from db with given username. Returns how many rows were affected. Currently won't
     * catch if affected rows were 0 or > 1 which maybe should be detected later? TODO remember to check if this is an issue
     * @param username user to be deleted
     */
    public boolean removeUser(String username) {
        boolean removed = false;
        SQLiteDatabase readable = instance.getReadableDatabase();
        String selection = USERS_USERNAME + " LIKE ?";
        String[] selectionArgs = {username};
        int removedRows = readable.delete(TABLE_USERS, selection, selectionArgs);
        if (removedRows == 1) removed = true;

        return removed;
    }

    /**
     * Updates given users password.
     * @param username user
     * @param password new password
     * @return if update was successful
     */
    public boolean updatePassword(String username, String password) {
        boolean updated = false;
        SQLiteDatabase readable = instance.getReadableDatabase();
        String selection = USERS_USERNAME + " LIKE ?";
        String[] selectionArgs = {username};
        ContentValues values = new ContentValues();
        values.put(USERS_PASSWORD, password);
        int rows = readable.update(TABLE_USERS, values, selection, selectionArgs);
        if (rows > 0) updated = true;
        return updated;
    }



    /** ----------------- HOUSE RELATED ------------------- */

    /**
     * Adds a new house into database. First checks if this exact house already exists. If so, will
     * return with false. If not, will continue to add a new house to given user.
     * @param housename name of the house
     * @param firstRoom name of the room
     * @param secondRoom name of the second room
     * @return If adding a new house was successful returns true, else returns false
     */
    public boolean addHouse(String housename, String user, String firstRoom, String secondRoom) {
        boolean houseAlreadyExists = this.checkHouse(housename, firstRoom, secondRoom);
        if (houseAlreadyExists) return false; //if house exists

        SQLiteDatabase writable = instance.getWritableDatabase();
        //New values for the new house with first room
        ContentValues values = new ContentValues();
        values.put(HOUSES_HOUSENAME, housename);
        values.put(HOUSES_USER, user);
        values.put(HOUSES_ROOM, firstRoom);
        //New values for the new house with second room
        ContentValues values2 = new ContentValues();
        values2.put(HOUSES_HOUSENAME, housename);
        values2.put(HOUSES_USER, user);
        values2.put(HOUSES_ROOM, secondRoom);

        long newId = writable.insert(TABLE_HOUSES, null, values);
        long newId2 = writable.insert(TABLE_HOUSES, null, values2);

        return (newId != -1 && newId2 != -1);
    }

    /**
     * Checks if this house already exists. Does not differentiate between house owners, only checks
     * if this house exists at all.
     * @param housename houses name
     * @param firstRoom room's name
     * @param secondRoom second room's name
     * @return true if this exact house exists
     */
    private boolean checkHouse(String housename, String firstRoom, String secondRoom) {
        boolean houseExists = false;
        //Making SQL query
        SQLiteDatabase readable = instance.getReadableDatabase();
        String columns[] = {HOUSES_HOUSENAME, HOUSES_ROOM};
        String selection = HOUSES_HOUSENAME + " LIKE ? AND " + HOUSES_ROOM + " LIKE ? AND " + HOUSES_ROOM + " LIKE ?";
        String[] selectionArgs = {housename, firstRoom, secondRoom};
        Cursor query = readable.query(TABLE_HOUSES, columns, selection, selectionArgs, null, null, null);
        query.moveToFirst();
        if (query.getCount() == 0 ) return houseExists;

        String house = query.getString(query.getColumnIndex(HOUSES_HOUSENAME));
        String room1 = query.getString(query.getColumnIndex(HOUSES_ROOM));
        query.moveToNext();
        String room2 = query.getString(query.getColumnIndex(HOUSES_ROOM));
        query.close();

        if (house != null && house.equals(housename)
                && room1 != null && room1.equals(firstRoom) && room2 != null && room2.equals(secondRoom))
            houseExists = true; //if this house already exists

        return houseExists;
    }

    /**
     * Returns an array of distinct houses with given username.
     * @param username users houses to get
     * @return array of houses
     */
    public String[] getHouses(String username) {
        SQLiteDatabase readable = instance.getReadableDatabase();
        String[] columns = {HOUSES_HOUSENAME};
        String selection = HOUSES_USER + " LIKE ?";
        String[] selectionArgs = {username};
        Cursor query = readable.query(true, TABLE_HOUSES, columns, selection, selectionArgs, null, null, null, null, null);
        query.moveToFirst();
        String[] houses = new String[query.getCount()];
        for (int i = 0; i < query.getCount(); i++) {
            String houseName = query.getString(query.getColumnIndex(HOUSES_HOUSENAME));
            query.moveToNext();
            houses[i] = houseName;
        }
        query.close();
        return houses;
    }

    /**
     * Removes houses from given user. Multiple houses are also possible to remove at once. Returns
     * boolean-values to indicate whether removal was possible. //TODO not the prettiest method, if enough time, do some touch-up
     * @param username user
     * @param houses users houses
     * @return if deletion was successful
     */
    public boolean deleteHouses(String username, ArrayList<String> houses) {
        boolean removed = false;
        SQLiteDatabase readable = instance.getReadableDatabase();
        String selection = HOUSES_USER + " LIKE ? AND " + HOUSES_HOUSENAME + " LIKE ?";
        String[] selectionArgs = {username, null};
        while (houses.size() > 0) {
            selectionArgs[1] = houses.get(0);
            readable.delete(TABLE_HOUSES, selection, selectionArgs);
            houses.remove(0);
            removed = true;
        }
        return removed;
    }

    /**
     * Changes house's name. Return boolean to indicate if name was changed correctly.
     * @param housename house which name is being altered
     * @param newHousename new name for the house
     * @return if name was changed
     */
    public boolean changeHousename(String housename, String newHousename) {
        boolean updateSuccess = false;
        SQLiteDatabase writable = instance.getWritableDatabase();
        String selection = HOUSES_HOUSENAME + " LIKE ?";
        String[] selectionArgs = {housename};
        ContentValues values = new ContentValues();
        values.put(HOUSES_HOUSENAME, newHousename);
        int updatedRows = writable.update(TABLE_HOUSES, values, selection, selectionArgs);
        if (updatedRows > 0) updateSuccess = true;
        return updateSuccess;
    }

    /**
     * Adds user to a house in other words adds a new row in House-table with housename
     * and username stated in parameters.
     * @param housename house where user is being added
     * @param username user to add into house
     * @return if update was successful
     */
    public boolean addUserToHouse(String housename, String username) {
        boolean updateSuccess = false;
        //Check is the user to be added a "real" user
        boolean userExists = checkLogin(username);
        if (!userExists) return updateSuccess; //If there is no user with this username, return
        //Add user to house
        String[] rooms = getRooms(housename);
        SQLiteDatabase writable = instance.getWritableDatabase();
        ContentValues values1 = new ContentValues();
        values1.put(HOUSES_HOUSENAME, housename);
        values1.put(HOUSES_USER, username);
        values1.put(HOUSES_ROOM, rooms[0]);
        long newId = writable.insert(TABLE_HOUSES, null, values1);
        ContentValues values2 = new ContentValues();
        values2.put(HOUSES_HOUSENAME, housename);
        values2.put(HOUSES_USER, username);
        values2.put(HOUSES_ROOM, rooms[1]);
        long newId2 = writable.insert(TABLE_HOUSES, null, values2);
        if (newId != -1 && newId2 != -1) updateSuccess = true;
        return updateSuccess;
    }

    /**
     * Returns house's roomnames as a string array.
     * @param housename house of which rooms being fetched
     * @return array of roomnames
     */
    public String[] getRooms(String housename) {
        SQLiteDatabase readable = instance.getReadableDatabase();
        String[] columns = {HOUSES_HOUSENAME, HOUSES_ROOM};
        String selection = HOUSES_HOUSENAME + " LIKE ?";
        String[] selectionArgs = {housename};
        Cursor query = readable.query(TABLE_HOUSES, columns, selection, selectionArgs, null, null, null);
        query.moveToFirst();
        String[] rooms = new String[query.getCount()];
        for (int i = 0; i < query.getCount(); i++) {
            String roomName = query.getString(query.getColumnIndex(HOUSES_ROOM));
            query.moveToNext();
            rooms[i] = roomName;
        }
        query.close();
        return rooms;
    }

    /**
     * Modifies house's power, lights and heating depending on whichObject parameter.
     * 1 indicates house's powers, 2 house's lights and 3 heating
     * @param housename house where objects are
     * @param buttonChecked if power or lights are being changed, this indicates their buttons and whether they are ON or OFF
     * @param whichObject which object is being modified
     * @param progressForHeating house heating's status
     */
    public void modifyHouseObjs(String housename, boolean buttonChecked, int whichObject, int progressForHeating) {
        String[] rooms = getRooms(housename); //Fetch all rooms of the given house
        String onOrOff = "0"; //Help variable for button status

        //Depending on which object is being modified, do updates to database
        switch (whichObject) {
            case (1): //House power
                if (buttonChecked) onOrOff = "1";
                for (String room : rooms) {
                    String[] items = getItems(room);
                    for (String item : items) {
                        changeObjectValue(room, item, onOrOff);
                    }
                }
                break;
            case (2): //House lights
                if (buttonChecked) onOrOff = "1";
                for (String room : rooms) {
                    changeObjectValue(room, "Valot", onOrOff);
                }
                break;
            case (3): //Heating
                for (String room : rooms) {
                    changeObjectValue(room, "Lämmitys", ("" + progressForHeating));
                }
                break;
        }
    }



    /** ----------------- ROOM RELATED ------------------- */

    /**
     * Changes room's name. Return boolean to indicate if name was changed correctly.
     * @param housename current house
     * @param roomname room which name is being altered
     * @param newRoomname new name for the room
     * @return if update was successful
     */
    public boolean changeRoomname(String housename, String roomname, String newRoomname) {
        boolean updateSuccess = false;

        //Update roomname change to HOUSES_TABLE
        SQLiteDatabase writable = instance.getWritableDatabase();
        String selectionH = HOUSES_HOUSENAME+ " LIKE ?" + HOUSES_ROOM + " LIKE ?";
        String[] selectionArgsH = {housename, roomname};
        ContentValues valuesH = new ContentValues();
        valuesH.put(HOUSES_ROOM, newRoomname);
        int updatedRowsH = writable.update(TABLE_HOUSES, valuesH, selectionH, selectionArgsH);

        //Update roomname change to ITEM_TABLE
        String selectionI = ITEMS_ROOM + " LIKE ?";
        String[] selectionArgsI = {roomname};
        ContentValues valuesI = new ContentValues();
        valuesI.put(ITEMS_ROOM, newRoomname);
        int updatedRowsI = writable.update(TABLE_HOUSES, valuesI, selectionI, selectionArgsI);

        if (updatedRowsH > 0 && updatedRowsI > 0) updateSuccess = true;
        return updateSuccess;
    }

    /**
     * Returns an array of items with given room name.
     * @param roomname room which items to get
     * @return array of items
     */
    public String[] getItems(String roomname) {
        SQLiteDatabase readable = instance.getReadableDatabase();
        String[] columns = {ITEMS_ROOM, ITEMS_ITEMNAME};
        String selection = ITEMS_ROOM + " LIKE ?";
        String[] selectionArgs = {roomname};
        Cursor query = readable.query(TABLE_ITEMS, columns, selection, selectionArgs, null, null, null);
        query.moveToFirst();
        String[] items = new String[query.getCount()];
        for (int i = 0; i < query.getCount(); i++) {
            String itemName = query.getString(query.getColumnIndex(ITEMS_ITEMNAME));
            query.moveToNext();
            items[i] = itemName;
        }
        query.close();
        return items;
    }

    /**
     * Modifies room's power, lights and heating depending on whichObject parameter.
     * 1 indicates room's powers, 2 room's lights and 3 heating
     * @param roomname house where objects are
     * @param buttonChecked if power or lights are being changed, this indicates their buttons and whether they are ON or OFF
     * @param whichObject which object is being modified
     * @param progressForHeating room heating's status
     */
    public void modifyRoomObjs(String roomname, boolean buttonChecked, int whichObject, int progressForHeating) {
        String[] items = getItems(roomname); //Fetch all items of given room
        String onOrOff = "0"; //Help variable for button status

        //Depending on which object is being modified, do updates to database
        switch (whichObject) {
            case (1): //Room power
                if (buttonChecked) onOrOff = "1";
                for (String item : items) {
                    changeObjectValue(roomname, item, onOrOff);
                }
                break;
            case (2): //Room lights
                if (buttonChecked) onOrOff = "1";
                changeObjectValue(roomname, "Valot", onOrOff);
                break;
            case (3): //Room heating
                changeObjectValue(roomname, "Lämmitys", ("" + progressForHeating));
                break;
        }
    }


    /** ----------------- ITEM/OBJECT RELATED ------------------- */

    /**
     * Method for adding default objects to house. Called while creating a new house in AddHouseActivity.
     * @return if database insertions were a success
     */
    public boolean addDefaultObjects(String firstRoom, String secondRoom) {

        SQLiteDatabase writable = instance.getWritableDatabase();

        //Values for first room's first default object
        ContentValues room1values1 = new ContentValues();
        room1values1.put(ITEMS_ITEMNAME, "Valot");
        room1values1.put(ITEMS_ROOM, firstRoom);
        room1values1.put(ITEMS_VALUE, "0");

        //Values for first room's second default object
        ContentValues room1values2 = new ContentValues();
        room1values2.put(ITEMS_ITEMNAME, "Lämmitys");
        room1values2.put(ITEMS_ROOM, firstRoom);
        room1values2.put(ITEMS_VALUE, "0");

        //Values for second room's first default object
        ContentValues room2values1 = new ContentValues();
        room2values1.put(ITEMS_ITEMNAME, "Valot");
        room2values1.put(ITEMS_ROOM, secondRoom);
        room2values1.put(ITEMS_VALUE, "0");

        //Values for second room's second default object
        ContentValues room2values2 = new ContentValues();
        room2values2.put(ITEMS_ITEMNAME, "Lämmitys");
        room2values2.put(ITEMS_ROOM, secondRoom);
        room2values2.put(ITEMS_VALUE, "0");

        long room1try1 = writable.insert(TABLE_ITEMS, null, room1values1);
        long room1try2 = writable.insert(TABLE_ITEMS, null, room1values2);
        long room2try1 = writable.insert(TABLE_ITEMS, null, room2values1);
        long room2try2 = writable.insert(TABLE_ITEMS, null, room2values2);

        return (room1try1 != -1 && room1try2 != -1 && room2try1 != -1 && room2try2 != -1);
    }

    /**
     * Method for adding custom items with objects to house. Called while creating a new house.
     * @param firstRoom first room's name
     * @param secondRoom second room's name
     * @param room1Item1Name first room's first item
     * @param room1Item2Name first room's second item
     * @param room2Item1Name second room's first item
     * @param room2Item2Name second room's second item
     * @return if inserts were successful
     */
    public boolean addItems (String firstRoom, String secondRoom, String room1Item1Name, String room1Item2Name,
                             String room2Item1Name, String room2Item2Name) {

        SQLiteDatabase writable = instance.getWritableDatabase();

        //Values for first room's first custom object
        ContentValues firsRoomItem1Values = new ContentValues();
        firsRoomItem1Values.put(ITEMS_ITEMNAME, room1Item1Name);
        firsRoomItem1Values.put(ITEMS_ROOM, firstRoom);
        firsRoomItem1Values.put(ITEMS_VALUE, "0");

        //Values for first room's second custom object
        ContentValues firstRoomItem2Values = new ContentValues();
        firstRoomItem2Values.put(ITEMS_ITEMNAME, room1Item2Name);
        firstRoomItem2Values.put(ITEMS_ROOM, firstRoom);
        firstRoomItem2Values.put(ITEMS_VALUE, "0");

        //Values for second room's first custom object
        ContentValues secondRoomItem1Values = new ContentValues();
        secondRoomItem1Values.put(ITEMS_ITEMNAME, room2Item1Name);
        secondRoomItem1Values.put(ITEMS_ROOM, secondRoom);
        secondRoomItem1Values.put(ITEMS_VALUE, "0");

        //Values for second room's second custom object
        ContentValues secondRoomItem2Values = new ContentValues();
        secondRoomItem2Values.put(ITEMS_ITEMNAME, room2Item2Name);
        secondRoomItem2Values.put(ITEMS_ROOM, secondRoom);
        secondRoomItem2Values.put(ITEMS_VALUE, "0");

        long room1try1 = writable.insert(TABLE_ITEMS, null, firsRoomItem1Values);
        long room1try2 = writable.insert(TABLE_ITEMS, null, firstRoomItem2Values);
        long room2try1 = writable.insert(TABLE_ITEMS, null, secondRoomItem1Values);
        long room2try2 = writable.insert(TABLE_ITEMS, null, secondRoomItem2Values);

        return (room1try1 != -1 && room1try2 != -1 && room2try1 != -1 && room2try2 != -1);
    }

    /**
     * Changes custom item's name.
     * @param roomname room where item is
     * @param itemname item which name is being altered
     * @param newItemname new name for the item
     * @return if update was successful
     */
    public boolean changeItemname(String roomname, String itemname, String newItemname) {
        boolean updateSuccess = false;
        SQLiteDatabase writable = instance.getWritableDatabase();
        String selection = ITEMS_ITEMNAME + " LIKE ?" + ITEMS_ROOM + " LIKE ?";
        String[] selectionArgs = {itemname, roomname};
        ContentValues values = new ContentValues();
        values.put(ITEMS_ITEMNAME, newItemname);
        int updatedRows = writable.update(TABLE_ITEMS, values, selection, selectionArgs);
        if (updatedRows > 0) updateSuccess = true;
        return updateSuccess;
    }

    /**
     * Changes object's/item's value. Returns boolean to indicate if update was successful.
     * @param roomname room where object is
     * @param objectName object/item which value to modify
     * @param newValue new value
     * @return if update was successful
     */
    public boolean changeObjectValue(String roomname, String objectName, String newValue) {
        boolean updateSuccess = false;
        SQLiteDatabase writable = instance.getWritableDatabase();
        String selection = ITEMS_ROOM + " LIKE ? AND " + ITEMS_ITEMNAME + " LIKE ?";
        String[] selectionArgs = {roomname, objectName};
        ContentValues values = new ContentValues();
        values.put(ITEMS_VALUE, newValue);
        int updatedRows = writable.update(TABLE_ITEMS, values, selection, selectionArgs);
        if (updatedRows > 0) updateSuccess = true;
        return updateSuccess;
    }

    /**
     * Returns given items value in database.
     * @param roomname room where object is
     * @param itemname item to be queried
     * @return value of item in integers
     */
    public int getObjectValue(String roomname, String itemname) {
        int value = 0;
        SQLiteDatabase readable = instance.getReadableDatabase();
        String[] columns = {ITEMS_VALUE};
        String selection = ITEMS_ROOM + " LIKE ? AND " + ITEMS_ITEMNAME + " LIKE ?";
        String[] selectionArgs = {roomname, itemname};
        Cursor query = readable.query(TABLE_ITEMS, columns, selection, selectionArgs, null, null, null);
        query.moveToFirst();
        value = query.getInt(query.getColumnIndex(ITEMS_VALUE));
        query.close();
        return value;
    }

    /**
     * Returns overall power in given room. Checks powers in every item except heating.
     * @param roomname room to check
     * @return true, if every item is on
     */
    public boolean getAllPowerRoom(String roomname) {
        boolean power = true;
        String[] items = getItems(roomname);
        for (String item : items) {
            if (item.equals("Lämmitys")) continue;
            if (getObjectValue(roomname, item) != 1) {
                power = false;
                break;
            }
        }
        return power;
    }

    /**
     * Returns overall power in whole house. If even one room has all power set off, will return false.
     * @param housename house to check
     * @return true if every room has every item on (except heating)
     */
    public boolean getAllPowerHouse(String housename) {
        boolean power = true;
        String[] rooms = getRooms(housename);
        for (String room : rooms) {
            if (!getAllPowerRoom(room)) {
                power = false;
                break;
            }
        }
        return power;
    }

    /**
     * Returns all lights in house.
     * @param housename house to check
     * @return true if every light is on in every room
     */
    public boolean getAllLightsHouse(String housename) {
        boolean lights = true;
        String[] rooms = getRooms(housename);
        for (String room : rooms) {
            if (getObjectValue(room, "Valot") != 1) {
                lights = false;
                break;
            }
        }
        return lights;
    }

    /**
     * Returns average of house's heating
     * @param housename house to check
     * @return integer value of whole house's heating
     */
    public int getAllHeat(String housename) {
        int heat = 0;
        String[] rooms = getRooms(housename);
        for (String room : rooms) {
            heat += getObjectValue(room, "Lämmitys");
        }
        heat = heat/2;
        return heat;
    }
}
