package com.example.lol.superkamppa;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

/**
 * Created by kkeikei on 3.3.2016.
 */
public class CreateUserScreen extends AppCompatActivity{

    private DBhelper dbHelper;
    private String newUser;
    private String newUserPassw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_user_screen);

        dbHelper = DBhelper.getInstance(this);

    }

    /**
     * Method to create a new user, admin or not. Method is called from create_user_screen
     * @param view
     */
    public void createUser (View view) {
        EditText username = (EditText) findViewById(R.id.new_user);
        EditText password = (EditText) findViewById(R.id.new_user_passw);
        ToggleButton admin = (ToggleButton) findViewById(R.id.admin_button);

        newUser = username.getText().toString();
        newUserPassw = password.getText().toString();
        Boolean newAdmin = admin.isChecked();
        if (!newAdmin && !dbHelper.queryAdmin()) {
            Toast.makeText(CreateUserScreen.this, "Luo ensin ylläpitäjä", Toast.LENGTH_LONG).show();
            return;
        }
        Boolean successful = dbHelper.addUser(newUser, newUserPassw, newAdmin);

        //Jos ylläpitäjän luonti onnistui, palataan edelliseen ikkunaan
        if (newAdmin && successful) {
            Toast.makeText(CreateUserScreen.this, "Ylläpitäjä luotu", Toast.LENGTH_LONG).show();
        } else if (!newAdmin && successful) { //Jos käyttäjän luonti onnistui, palataan edelliseen ikkunaan
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Talon luonti")
                    .setMessage("Käyttäjä luotu, haluatko luoda hänelle talon?")
                    .setPositiveButton("Haluan", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(CreateUserScreen.this, AddHouseActivity.class);
                            intent.putExtra(AddHouseActivity.EXTRA_USER, CreateUserScreen.this.newUser);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("En halua", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            CreateUserScreen.this.finish();
                        }
                    })
                    .show();

        } else { //Jos mikään ei onnistunut, voidaan yrittää uudestaan tai palata edelliseen ikkunaan
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            String message = "";
            if (newAdmin) message = "Ylläpitäjän luonti ei onnistunut";
            else message = "Käyttäjän luonti ei onnistunut";
            builder.setMessage(message);
            builder.setPositiveButton("Yritä uudestaan", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    CreateUserScreen.this.finish();
//                    Intent intent = new Intent(CreateUserScreen.this, CreateUserScreen.class);
                }
            });
            builder.setNegativeButton("Poistu", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    CreateUserScreen.this.finish();
                }
            });
            builder.show();
        }
    }
}

