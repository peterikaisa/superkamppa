package com.example.lol.superkamppa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    private DBhelper dbHelper;
    public final static String EXTRA_USER = "com.example.lol.superkamppa.USER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbHelper = DBhelper.getInstance(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * Method to call when login-button is pushed. Credentials will be matched with
     * database and determine which activity is opened. Method is called from login_button in
     * activity_main
     * @param view
     */
    public void login(View view) {

        EditText username = (EditText) findViewById(R.id.username_login);
        String user = username.getText().toString();
        EditText passw = (EditText) findViewById(R.id.username_password);
        String password = passw.getText().toString();
        boolean[] answer = dbHelper.checkLogin(user, password);
        if (answer[1] && answer[0]) { //If user is an admin
            Intent intent = new Intent(this, AdminScreen.class);
            intent.putExtra(EXTRA_USER, user);
            startActivity(intent);
        } else if (answer[0]) { //Regular user
            Intent intent = new Intent(this, UserScreen.class);
            intent.putExtra(EXTRA_USER, user);
            startActivity(intent);
        } else {
            Toast.makeText(MainActivity.this, "Käyttäjää ei ole", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Creates a new admin, if device doesn't yet have one. Method is called from create_admin_button in activity_main
     * @param view
     */
    public void createAdmin(View view) {
        if (dbHelper.queryAdmin()) {
            Toast.makeText(MainActivity.this, "Ylläpitäjä on jo olemassa", Toast.LENGTH_LONG).show();
        } else {
            Intent intent = new Intent(this, CreateUserScreen.class);
            startActivity(intent);
        }
    }

}

